import UIKit

/*
 замыкания являются аноинимными функциями которые можно передавать
 в качестве аргументов другой функции
 {
 параметры -> возвращаемый_тип in
                (операторы)
 }
 */

func nameF(a: String) {
    
}

let names = ["Ma", "Serge", "Nikola", "Atrem", "albertinio", "Tom"]
let sortedNames = names.sorted()

let t1 = names.sorted { (s1: String, s2: String) -> Bool in
    return s1 < s2
}

let t2 = names.sorted {
    (s1, s2) in return s1 < s2
}

let t3 = names.sorted { (s1, s2) in s1 < s2 }

let t5 = names.sorted { (s1, s2) in
    s1.count < s2.count
}

//автоматические имена аргументов

let  p1 = names.sorted(by: {
                        $0 < $1 })

let  p2 = names.sorted(by: {
                        $0.count < $1.count })

//захват значения

func makeTranslator(stroka: String) -> (String) -> (String) {
    return {(name: String) -> String in return (stroka + " " + name)}
}

var englishWelcome = makeTranslator(stroka: "Hellow!")

englishWelcome("Jack")
englishWelcome("paul"   )
